#!/bin/bash

MISSING_PARAMS=false

if [[ -z "${GUID}" ]]; then
  ENV_GUID=$(hostname | cut -d"." -f2)
else
  ENV_GUID="${GUID}"
fi

if [[ -z "${oreg_auth_user}" ]]; then
  echo 
  echo "You must export "oreg_auth_user" variable with valid service account, export oreg_auth_user=\"value\""
  echo 
  MISSING_PARAMS=true
else
  ENV_OREG_AUTH_USER="${oreg_auth_user}"
fi

if [[ -z "${oreg_auth_password}" ]]; then
  echo
  echo "You must export "oreg_auth_password" variable with valid service account token, export oreg_auth_password=\"value\""
  echo
  MISSING_PARAMS=true
else
  ENV_OREG_AUTH_PASSWORD="${oreg_auth_password}"
fi

if [ "$MISSING_PARAMS" == true ] ; then
  echo "Aborting exectution because of missing export variables."
  exit 1
fi

echo "####################"
echo "Bootstrapping up environment based on GUID: ${ENV_GUID}"
echo "--------------------"
echo "ansible-playbook generate_hostfile.yml --extra-vars \"guid=${ENV_GUID}\""
echo "####################"
echo
ansible-playbook generate_hostfile.yml --extra-vars "guid=${ENV_GUID} oreg_auth_user=${ENV_OREG_AUTH_USER} oreg_auth_password=${ENV_OREG_AUTH_PASSWORD}"
echo "Copying generated config to /etc/ansible/hosts"
echo
cp /etc/ansible/hosts /etc/ansible/hosts.$(date +%Y%m%d-%H%M%S).ini
cp inventory/hosts.ini /etc/ansible/hosts
echo "####################"
echo "Running Prerequisites"
echo "####################"
echo
ansible-playbook /usr/share/ansible/openshift-ansible/playbooks/prerequisites.yml
echo "####################"
echo "Running cluster deploy"
echo "####################"
echo
ansible-playbook /usr/share/ansible/openshift-ansible/playbooks/deploy_cluster.yml
echo "####################"
echo "Copying .kube/config"
echo "####################"
echo
ansible masters[0] -b -m fetch -a "src=/root/.kube/config dest=/root/.kube/config flat=yes"

echo "####################"
echo "Sanity checks"
echo "####################"
echo
oc whoami
oc get nodes --show-labels
oc scale -n default --replicas=2 dc/router

echo "####################"
echo "POST Intall: nfs share volumes "
echo "####################"
echo
ansible-playbook create_nfsshares.yml
./create_volumes.sh
cat pvs/* | oc create -f -

echo  
echo "Volume Claims and persistance volumes"
echo
oc get pvc
oc get pv
echo 
echo "Creating test project and creating an nodejs app"
echo
oc new-project sanity-check --display-name="Sanity Check" --description="Sanity Check deployment of NodeJS app"
oc project sanity-check
oc new-app nodejs-mongo-persistent
oc get svc
oc get routes

