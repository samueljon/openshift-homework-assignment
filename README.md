# Openshift homework assignment

* __Instructor:__ Jindřich Káňa
* __Class Locaction:__ Kista, Stockholm, Sweden
* __Class Date:__ 26-30 November, 2018
* __Student:__ Samúel Jón Gunnarsson <samuel.jon.gunnarsson@gmail.com> , <samuel@ok.is>

## Bug hunt results

The first five issues where found during inventory inspection prior to running the ```prerequisites.yaml``` and ```deploy_cluster.yaml```. 

* Issue #1 - Infra and master nodes in wrong groups, see: f9786831aaa5d0768e9a7d5b6f78f3bdba782a0f
* Issue #2 - Cluster Metrics nfs server incorrectly specified, see: 48ef46da232e49f6973b7099ae5807a0918b5abc
* Issue #3 - oreg_auth parameters missing in the inventory file, see: 3c5efd2fa1c5ea3a1d73fe613ad2f5bf359abc48
* Issue #4 - openshift public hostname not correct, see: 91a5c7dc70e15e408f10937f509e3db8de41ccc2
* Issue #5 - mismatch in runtime definitions, see: 5fa2a8798e306b918f81b71c54ba8818e9da6ac7
* Issue 6 -  oreg_url component missing $ sign see: 7b2a908cf4c6a7473470f9819a4114470a7d8266 
* Issue 7 -  registry is referencing glusterfs but should be nfs, see: 4f7f816228104f58e909cdf8401641858df43022
* Issue 8 -  Added persistent storage support, see 61864815fcdad418751dca60ac92fba34494a1a3 

The final version of the hosts file can be reviewed here: https://gitlab.com/samueljon/openshift-homework-assignment/blob/master/hosts.ini and commits 9431cd5a9262a867f73169973a5c6625699f207b...0da793808d37683cd4f6a19f866bbf692519e68a show the change history.

## Requirements

- [x] Ability to authenticate at the master console. Validated with: ```User: Andrew Password: r3dh4t1!```
- [x] Registry has storage attached and working. Validated with ```oc get pv``` and ```oc get pvc```
- [x] Router is configured on each infranode. Validated with ```oc scale -n default --replicas=2 dc/router``` and ```oc get dc/router -n default```
- [x] Ability to deploy a simple app (nodejs-mongodb), Validated with ```oc new-app nodejs-mongo-persistent```
- [x] There are three master control plane nodes working. Validated with ```oc get pods -n kube-system -o wide```
- [x] There is a load balancer to access the masters called loadbalancer.$GUID.$DOMAIN. Validated with logging into the management console at https://loadbalancer.$GUID.$DOMAIN
- [x] There is DNS round-robin for both infranodes called *.apps.$GUID.$DOMAIN. Validated with ```dig ANY nodejs-ex-sanity-check.apps.718e.example.opentlc.com```
- [x] There are at least two infranodes, in node_group node-config-infra labeled node-role.kubernetes.io/infra=true', validated with ```oc get pods -o wide``` and observing amount of infra nodes.
- [x] NetworkPolicy SDN plugin is configured, Validated with ```oc get clusternetwork```
- [x] Aggregated logging is configured and working, validated with ```oc get events``` 
- [x] Metrics (Prometheus and Hawkular) collection and graphing is configured and working. Validated under monitoring in projects and under https://prometheus-k8s-openshift-monitoring.apps.718e.example.opentlc.com , https://grafana-openshift-monitoring.apps.718e.example.opentlc.com
- [x] Router and Registry Pods run on Infranodes, validated with ```oc get pods -o wide``` and observing amount of routers and placement.
- [x] Metrics and Logging components run on Infranodes, validated through cluster console.
- [x] Service Catalog, Template Service Broker, and Ansible Service Broker are all working. Validated by deploying an app from the service catalog. 
- [x] Operator Lifecycle Manager is configured and working, validated through cluster console.
- [x] Authentication to the Red Hat Registry (registry.redhat.io) works when the customer adds credentials to the inventory file. Validated with temporary credentials.
- [x] Additional registries are accessible from OpenShift. Validated with operator framework registries, see Operator Lifecycle Manager requirement above.

## Boostrapping the cluster 

This section describes how to run the scripts in the repository in order to validate the installation from the inventory file. The bootstrap script uses the inventory file as a template that is updated with ```${GUID}``` in the lab environment that the script will be executed.

### Assumptions and prerequisites

:warning:  The scripts assume that the following environment has been provisoned and that the scripts are to be runned on the bastion host as ```root```. You must also have a service account @ https://access.redhat.com/ that will be used to pull images from that registry. Service Accounts can be created here: https://access.redhat.com/terms-based-registry/

__Provisioned Environment Hosts__
* __Bastion host:__ bastion.$GUID.example.opentlc.com and bastion.$GUID.internal
* __Load balancer:__ loadbalancer.$GUID.example.opentlc.com and loadbalancer.$GUID.internal
* __3 OpenShift master nodes:__ master{1-3}.$GUID.internal
* __2 OpenShift infrastructure nodes:__ infranode{1,2}.$GUID.example.opentlc.com and infranode{1,2}.$GUID.internal
* __3 OpenShift worker nodes:__ node{1-3}.$GUID.internal

### Running the scripts



```shell
ssh bastion.$GUID.example.opentlc.com 
sudo su - 
git clone https://gitlab.com/samueljon/openshift-homework-assignment.git
cd openshift-homework-assignment
export oreg_auth_user="value"
export oreg_auth_password="value"
./bootstrap.sh
```

## Tearing down the cluster

The cluster can be teardown with the teardown script ( ```teardown.sh``` ) within the ```openshift-homework-assignment``` folder if you want to uninstall or reinstall the cluster. 

:warning: All DATA and CONFIG will be deleted. 
