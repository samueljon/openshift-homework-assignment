#!/bin/bash
#
# Author: Samúel Jón Gunnarsson <samuel.jon.gunnarsson@gmail.com>
#
# Description:
# Tears down the environment f.ex. if you want to reinstall or retest


echo "Running Ansible uninstall playbook"
ansible-playbook /usr/share/ansible/openshift-ansible/playbooks/adhoc/uninstall.yml
echo "Removing config directory"
ansible nodes -a "rm -rf /etc/origin"
echo "Removing data on nfs server"
ansible nfs -a "rm -rf /srv/nfs/*"
echo "Removing .Kube folder on the bastion host"
rm -fr $HOME/.kube
